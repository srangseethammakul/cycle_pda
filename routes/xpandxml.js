var express = require('express');
var parser	=require('xml2json-light');
var router = express.Router();
var db 		=require('../resource/dbconnect')
/* GET users listing. */
 
router.get('/UPSTREAM/:cloud/:rec' , function (req, res ) {
	 var recid=req.params.rec;
	 var rm =req.params.cloud;
	 var qry="Select * from CLOUDPOS..UPSTREAM Where RECID=" +recid;
	 if(rm==1)qry ="use.PLPPSRV^ select * from POSXFER..UPSTREAM Where RECID=" +recid;
	 
	 db.dbGetQuery(qry,function(resp){
		 try { 
			 //console.log(resp);
			    var emsg=resp.dbmessage;
			    var ecode=resp.dbcode;
			    if((ecode==0 )&& (resp.dbitems.length>0 ) ){
					var rset =resp.dbitems;  //--result set from DB
					var art =rset[0].ARTICLE;
					var stt =rset[0].STATUS;
					var mdt =rset[0].MDATE;
					var stc =rset[0].STCODE;
					var xml =rset[0].XMLDATA;
					//-----Convert XMLDAte to JSON
				
					var json = parser.xml2json(xml);
					var rjson ={
							recid:recid,
							article:art,
							mdate:mdt,
							status:stt,
							xmldata:json
					}
					res.status(200).json(rjson);
			    }
			    else {
			    	res.send('DB Error... Can not GEt UPSTREAM Rec '+recid) ;
			    }

		   }
		catch(ex){res.send('Expand Error...'+ex.message);}
	 } );
});
module.exports = router;
