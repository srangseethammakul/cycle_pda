var express = require('express');
var router 	= express.Router();
var request  =require('request')
/* GET product service */
var db		=require('../resource/dbconnect')
//var cld		=require('../resource/cldconnect')
var dt		=require('../resource/datetime')


router.get('/LOGOUT/:cid', function (req, res ) {	 
	 var stc 	=req.params.stc;
	 var cid	=req.params.cid;
	 var lck 	=req.params.lck;
	 //----------Storecode alway from this noe service
	 
	 console.log("req to Logout  ");
	//    ws_cashierLogout
		var qry ="exec ws_userLogOut  '"+cid+"'  ";
		 db.dbGetQuery(qry,function(ret){
			 console.log(ret);
			 res.status(200).json(ret);
		 });
});

router.get('/LOGIN/:cid/:pwd', function (req, res ) {	 
	 var cid	=req.params.cid;
	 var pwd	=req.params.pwd;
	 var ip = req.headers['x-forwarded-for'] || 
		     req.connection.remoteAddress || 
		     req.socket.remoteAddress ||
		     (req.connection.socket ? req.connection.socket.remoteAddress : null);
	  //--remove ::ffff: if any 
	   ip=ip.replace('::ffff:','');
	   console.log("req to LogIn  "+cid );
		var qry ="exec ws_userLogin '"+ip+"','" +cid +"','" +pwd +"'"  ;
		
		    db.dbGetQuery(qry,function(ret){
			// console.log(ret);
			 res.status(200).json(ret);
		 });
});



///------------------ STAFF LOGIN
router.get('/STAFFLOGIN/:cid/:pwd', function (req, res ) {	 
	 var cid	=req.params.cid;
	 var pwd	=req.params.pwd;
	 var ip = req.headers['x-forwarded-for'] || 
		     req.connection.remoteAddress || 
		     req.socket.remoteAddress ||
		     (req.connection.socket ? req.connection.socket.remoteAddress : null);
	  //--remove ::ffff: if any 
	   ip=ip.replace('::ffff:','');
	   console.log("staff req to login  "+cid );
		var qry ="exec ws_Stafflogin '"+ip+"','" +cid +"','" +pwd +"'"  ;
		
		    db.dbGetQuery(qry,function(ret){
			// console.log(ret);
			 res.status(200).json(ret);
		 });
});


//--------------Check Scan ID CARD ----
router.get('/IDCARD/:card', function (req, res ) {	 
	 var card=req.params.card;
	 var ip = req.headers['x-forwarded-for'] || 
	     req.connection.remoteAddress || 
	     req.socket.remoteAddress ||
	     (req.connection.socket ? req.connection.socket.remoteAddress : null);

	 var qry ="exec ws_checkidcard '"+ ip+ "','"+ card +"'";
	 db.dbGetQuery(qry,function(ret){
		 console.log(ret);
		 res.status(200).json(ret);
	 });
	});

//--------------Save Current Receipt ----
router.get('/SAVE/:stcode/:lock', function (req, res ) {	 
 	;
 	 var stc =req.params.stcode;
 	 var lck =req.params.lock;
 	 console.log('Save to temp '+stc+lck );
	 var qry ="exec ws_save2temp '"+ stc + "','"+ lck +"'";
	 db.dbGetQuery(qry,function(resp){
		// console.log(resp);
		 res.status(200).json(resp);
	   });
	});
router.get('/RECALL/:stcode/:lock' , function (req, res) {
 	console.log('Save to temp '+req); 
	 var stc =req.params.stcode;
	 var lck =req.params.lock;
	 var qry ="exec ws_recalltemp '"+ stc + "','"+ lck +"'";
	 db.dbGetQuery(qry,function(resp){
		 res.status(200).json(resp);
	   });
       
});
//----------read what customer sacan to POS
router.get('/CSCANIN/:cref/:stcode/:lock' , function (req, res) {
 	console.log('Save to temp '+req); 
	 var stc =req.params.stcode;
	 var lck =req.params.lock;
	 var ref =req.params.cref;
	 var qry ="exec cs_scan2sales '" +ref +"', '"+ stc + "','"+ lck +"'";
	 db.dbGetQuery(qry,function(resp){
		 res.status(200).json(resp);
	   });
       
});

//----------Show Receipt
router.get('/RCPT/:rcpt' , function (req, res) {
 	console.log('Edit xtrans Item '+req); 
	 var rcpt =req.params.rcpt;
 	 var qry ="exec ws_showReceipt '"+rcpt +"'";
	 db.dbGetQuery(qry,function(resp){
		 
	   });
       
});

router.post('/QUERY' , function (req, res) {
	console.log('call POSSRV/QUERY');
	/**** Prevent call from Outside Domain **/
	var host=req.get('host').split(':')[0]; 	 
	var callfrom=req.headers.origin; 
	
	if(callfrom.indexOf(host)<=0) {
		console.log(' Ilegall call from '+ req.headers.origint+', to Server');
		res.status(404).end('Call Error'); 
		return;
	} 

	/**************************** */
	if(!req.body.query){
		res.status(200).json({
			    dbcode:999,
				dbmessage:'query is not defined'  ,
				dbitems:null 
		       })
	    return; 
	}
	else { 
	 var qry=req.body.query;
	 var par=req.body.para;
	 //--replace parameter if any
	 if(par!=null){
		 for(key in par){
			 var rp =RegExp('@'+key,'g');
			 qry=qry.replace(rp,par[key]);
		 }
	  }
      db.dbGetQuery(qry,function(resp){
     	  // 	console.log(resp);
     		res.status(200).json(resp);
     	});
	}
});
router.post('/QRY2TAB' , function (req, res) {
    console.log('call POSSRV/QRY2TAB');
	if(!req.body.query){
		 res.send('query is not defined');
		 res.end();
		 return;
		}
	var qry=req.body.query;
    db.dbGetQuery(qry,function(resp){
		   try { 
     		var ecode=resp.dbcode;
     		if(ecode>0){
     			res.send('DB Error '+resp.dbmessage);
     			res.end();
     			return;
     		}
     		else {
     			var nrw=resp.dbitems.length;
     			if(nrw<=0){
     				res.send('No result from DB');
     				res.end();
     				return;
     			}
     			var htm="<table>";
     			for(var rw =0;rw<resp.dbitems.length;rw++){
     				htm+="<tr>";
     				for(key in resp.dbitems[rw]){
     					var val =resp.dbitems[rw][key];
     					htm +="<td>"+val+"</td>"
     				}
     				htm+="</tr>"
     				}
     			htm+="</table>";
     			res.send(htm);
     			res.end();
				 }
			 }
		catch(ex){
			res.send("Error..."+ex.message);
			res.end();
		}	 

     	});

});


router.post('/QRY2TABREPORT' , function (req, res) {
    console.log('call POSSRV/QRY2TABREPORT');
	if(!req.body.query){
		 res.send('query is not defined');
		 res.end();
		 return;
		}
	var qry=req.body.query;
    db.dbGetQuery(qry,function(resp){
     		var ecode=resp.dbcode;
     		if(ecode>0){
     			res.send('DB Error '+resp.dbmessage);
     			res.end();
     			return;
     		}
     		else {
				 
     			var nrw=resp.dbitems.length;
     			if(nrw<=0){
     				res.send('No result from DB');
     				res.end();
     				return;
				 }
				 
				 //console.log(resp);

				 var htm="<table id='myTableData' class='table'>";
				 for(var rw =0;rw<resp.dbfields.length;rw++){
					htm+="<thead>";
					for(key in resp.dbfields[rw]){
						var val =resp.dbfields[rw][key];
						htm +="<th>"+key+"</th>"
					}
					htm+="</thead>"
				}
				htm+="<tbody>"
     			for(var rw =0;rw<resp.dbitems.length;rw++){
     				htm+="<tr>";
     				for(key in resp.dbitems[rw]){
     					var val =resp.dbitems[rw][key];
     					htm +="<td>"+val+"</td>"
     				}
     				htm+="</tr>"
     			}
     			htm+="</tbody></table>";
     			res.send(htm);
     			res.end();
     			}
     	});

});

router.post('/POSREQ' , function (req, res) {
	//console.log("Request Body...");
	//console.log(req.body);
	//return;
	if(!req.body.query){
		res.status(200).json({
			error:{ 
				message:'query is not defined'  ,
				code:99
			},
			resp:null,
			time:dt.myDateTime()
		})
	return; 
	}
     var qry=req.body.query;
     	db.dbGetQuery(qry,function(resp){
     		//console.log(resp);
     		res.status(200).json({
				error:{ 
					message:resp.dbmessage ,
					code:resp.dbcode,
				   },
				resp:resp,
				time:dt.myDateTime()
				})
     	});

});
//---Copy Pim Product from Upfront PIM  to CLoud PIM
router.get('/PLPP/:stcode/:skcode' , function (req, res) {
	var skc 	= req.params.skcode;
	var stc 	= req.params.stcode;
    var url 	= inetgateway+'/INET/PLPP/' +stc+ '/'+skc;
    
    request.get(url, function(error, response, body){
    	console.log('PLPP ret '+error);
    	if(error)res.status(404).send(error);
    	else res.status(200).send(body);
    });
   
} )

//-----Check Conenction to Cloud ------
router.get('/RMCNN' , function (req, res) {  //--check Remote Connect
 	//console.log('Check RMCNN'+req); 
	  res.status(200).send("Connection OK");
	 /*
	 cld.chkCldConn('cldsrv',function(err,resp){
		// console.log(JSON.stringify(err));
		 if(err)res.status(404).send(err.message);
		 else res.status(200).send("Connection OK");
		 
	   });
       */
});
//-----------get Image of this SKCODE
router.get('/B/:skcode',function(req,res){
	  var  skc=req.params.skcode;
	  console.log('Req DB Image '+skc)
	  var qry= "select base64 from twdpim..PIMIMAGE where SKC ="+skc;
	  try {
	     	db.dbGetQuery(qry,function(resp){
	     		// console.log(resp);
	     		if(resp.dbcode==0 && resp.dbrows>0){
	     			var bimg=resp.dbitems[0].base64;
	     			res.status(200).send("<img src='data:image/jpeg;base64,"+ bimg +"' >");
	     		}
	     		else
	     			res.status(200).send("<img src='./public/images/noimg.png'>" );
	     	});
	  }
	  catch(ex){
		  	res.status(200).send('') }; 
	  }
		  
     ) ;

 
module.exports = router;
