
exports.msGetQuery = function(sql,callback){
			getQuery(sql,function(ret){
				callback(ret);
			  });
	  
};
const config = require('config');
const dbConfig = config.get('mysqlConfig');

function getQuery(sql,callback){

var mysql = require('mysql');
	var con = mysql.createConnection({
	  host:dbConfig.host,  // "ubu.upfront.co.th",
	  user: dbConfig.userid,
	  password:dbConfig.password // "upfront"
	});

	con.connect(function(err) {
	  if (err)throw(err);
	  console.log("Query MYSQL "+sql);
	});
	  
	var ecode =0;
	var emsg="OK";
	var fields =[];
	    
   con.query(sql, function (err, results,flds) {
		if (err){
			//---return error
		//	console.log("Connect Error .."+err);
			ecode	=err.errno;
			emsg	=err.sqlMessage;
			 var resp ={
					 dbcode:ecode,
					 dbmessage:emsg
		   			}
	   	    callback(resp);
			}
		
		else { 
			//console.log("result"+ results);
			for(var i=0;i<flds.length;i++){
				var cty ={ 
				   cname:flds[i].name,
				   ctype:flds[i].type,
				   clen:flds[i].length
					}
				fields.push(cty);
			}
			
	     var resp ={
 				 dbcode:ecode,
 				 dbmessage:emsg,
 				 dbfileds:fields,
 				 dbitems:results
 	   			}
	     
    	  callback(resp);
		}
	});
  con.end(function(err){
     if(err){ 
		console.log("Con end with Error.."+ err);
		//ecode	=err.errno;
		 emsg	="Error"; //err.sqlMessage;
		 var resp ={
				 dbcode:99,
				 dbmessage:emsg
	   			}
	     
   	   callback(resp);
   }
  });
}